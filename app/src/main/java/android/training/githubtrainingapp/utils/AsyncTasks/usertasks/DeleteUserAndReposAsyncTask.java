package android.training.githubtrainingapp.utils.AsyncTasks.usertasks;

import android.os.AsyncTask;
import android.training.githubtrainingapp.model.User;
import android.training.githubtrainingapp.utils.GitHubApp;
import android.util.Log;

public class DeleteUserAndReposAsyncTask extends AsyncTask<User,Void,Void> {
    private OnDeleteUserListener mOnDeleteUserListener;

    public DeleteUserAndReposAsyncTask(OnDeleteUserListener listener) {
        mOnDeleteUserListener = listener;
    }

    public interface OnDeleteUserListener{
        void onDeleteUserFinished();
    }

    @Override
    protected Void doInBackground(User... user) {
        GitHubApp.mDatabase.mUserDao().delete(user[0]);
        GitHubApp.mDatabase.mRepoDao().deleteAll();
        //test
        Log.d("test", "Deleted user: " + user[0].toString());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mOnDeleteUserListener.onDeleteUserFinished();
    }
}
