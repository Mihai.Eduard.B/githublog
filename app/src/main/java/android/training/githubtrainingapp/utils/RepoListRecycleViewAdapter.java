package android.training.githubtrainingapp.utils;


import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.training.githubtrainingapp.R;
import android.training.githubtrainingapp.controller.fragment.RepoDetailFragment;
import android.training.githubtrainingapp.model.Repo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.List;
import java.util.Objects;

public class RepoListRecycleViewAdapter extends RecyclerView.Adapter implements Filterable {
    public Repo mRepo;
    private List<Repo> mRepoList;
    private Filter mRepoFilter = new Filter() {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Repo> filteredList;
            String filterString = constraint.toString().toLowerCase().trim();
            if(filterString.length()>1){
                filteredList = GitHubApp.mDatabase.mRepoDao().findAllReposByWildcardName(filterString);
            }else{
                filteredList = GitHubApp.mDatabase.mRepoDao().getAllRepos();
            }

            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mRepoList.clear();
            mRepoList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    public RepoListRecycleViewAdapter(List<Repo> repoList) {
        setRepoList(repoList);
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.repo_list_view_holder, viewGroup, false);

        return new RepoListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        TextView textView = viewHolder.itemView.findViewById(R.id.view_holder_repo);
        textView.setText(mRepoList.get(i).getName());
        textView.setOnClickListener((v)->{
            mRepo = mRepoList.get(i);
            AppCompatActivity appCompatActivity = (AppCompatActivity) v.getContext();
            if (appCompatActivity.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
                Fragment fragment = appCompatActivity.getSupportFragmentManager().findFragmentById(R.id.land_repo_detail_fragment_container);
                if(fragment!=null){
                    assert fragment != null;
                    TextView repoDetailText = Objects.requireNonNull(fragment.getView()).findViewById(R.id.repo_details_text);
                    repoDetailText.setText(mRepo.getName() + " " + mRepo.getRepoOwner() + " Private Repo: " + mRepo.isPrivate());
                }else {
                    startRepoDetailFragment(i, appCompatActivity);
                }
            }else {
                startRepoDetailFragment(i, appCompatActivity);
            }
        });
    }



    private void startRepoDetailFragment(int i, AppCompatActivity appCompatActivity) {
        Fragment fragment = RepoDetailFragment.getInstance(mRepoList.get(i));
        appCompatActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.profile_fragment_container, fragment, RepoDetailFragment.TAG_REPO_DETAIL_FRAGMENT)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public int getItemCount() {
        return mRepoList.size();
    }




    public List<Repo> getRepoList() {
        return mRepoList;
    }

    public void setRepoList(List<Repo> repoList) {
        mRepoList = repoList;
    }

    public Repo getRepo() {
        return mRepo;
    }

    public void setRepo(Repo repo) {
        mRepo = repo;
    }

    @Override
    public Filter getFilter() {
        return mRepoFilter;
    }

}
