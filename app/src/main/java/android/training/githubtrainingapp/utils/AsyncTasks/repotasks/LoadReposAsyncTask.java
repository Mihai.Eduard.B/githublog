package android.training.githubtrainingapp.utils.AsyncTasks.repotasks;

import android.os.AsyncTask;
import android.training.githubtrainingapp.model.Repo;
import android.training.githubtrainingapp.utils.GitHubApp;
import android.training.githubtrainingapp.utils.RepoTypeEnum;
import android.util.Log;

import java.util.List;

public class LoadReposAsyncTask extends AsyncTask<String,Void, List<Repo>> {
    private OnLoadReposListener mOnLoadReposListener;

    public LoadReposAsyncTask(OnLoadReposListener onLoadReposListener) {
        mOnLoadReposListener = onLoadReposListener;
    }

    public interface OnLoadReposListener{
        void onLoadReposFinished(List<Repo> repos);
    }

    @Override
    protected List<Repo> doInBackground(String... strings) {
        String requiresPrivateString = strings[1];
        if(RepoTypeEnum.valueOf(requiresPrivateString) == RepoTypeEnum.ALL){
            return GitHubApp.mDatabase.mRepoDao().findAllByOwner(strings[0]);
        }else{
            boolean requiresPrivate = RepoTypeEnum.valueOf(requiresPrivateString)==RepoTypeEnum.PRIVATE;
            Log.d("test", "REQUIRES PRIVATE BOOL IN LOAD REPOS ASYNC TASK IS: " + requiresPrivate);
            return GitHubApp.mDatabase.mRepoDao().findAllByNameAndPrivacy(strings[0], requiresPrivate);
        }
    }

    @Override
    protected void onPostExecute(List<Repo> repos) {
        super.onPostExecute(repos);
        mOnLoadReposListener.onLoadReposFinished(repos);
    }
}
