package android.training.githubtrainingapp.utils;

import java.io.Serializable;

public enum RepoTypeEnum implements Serializable {PUBLIC, PRIVATE, ALL
}
