package android.training.githubtrainingapp.utils.AsyncTasks.repotasks;

import android.os.AsyncTask;
import android.training.githubtrainingapp.model.Repo;
import android.training.githubtrainingapp.utils.GitHubApp;

import java.util.List;

public class SaveReposAsyncTask extends AsyncTask<Repo, Void, Void> {

    private OnSaveRepoListener mOnSaveRepoListener;

    public SaveReposAsyncTask(OnSaveRepoListener onSaveRepoListener) {
        mOnSaveRepoListener = onSaveRepoListener;
    }

    public interface OnSaveRepoListener{
        void onSavedUserFinished();
    }

    @Override
    protected Void doInBackground(Repo... repos) {
        GitHubApp.mDatabase.mRepoDao().insertAll(repos);
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mOnSaveRepoListener.onSavedUserFinished();
    }
}
