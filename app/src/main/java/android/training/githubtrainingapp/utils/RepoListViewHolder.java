package android.training.githubtrainingapp.utils;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class RepoListViewHolder extends RecyclerView.ViewHolder {
    public View mView;

    public RepoListViewHolder(@NonNull View itemView) {
        super(itemView);
        mView = itemView;
    }
}
