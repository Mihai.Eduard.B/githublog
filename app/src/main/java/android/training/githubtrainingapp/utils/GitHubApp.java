package android.training.githubtrainingapp.utils;

import android.app.Application;
import android.arch.persistence.room.Room;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.training.githubtrainingapp.database.GitHubAppDatabase;
import android.util.Log;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;


public class GitHubApp extends Application {
    public static final String KEY_USERNAME_SAVED = "key_username_saved";
    public static final String KEY_PASSWORD_SAVED = "key_password_saved";
    public static final String KEY_SHARED_PREFERENCE = "key_shared_preference";
    public static final String KEY_USERNAME_TO_PROFILE_SCREEN = "key_username_to_profile_screen";

    public static String mCurrentUser = "";
    public static String mCurrentUserPassword = "";
    public static GitHubAppDatabase mDatabase;
    public static boolean mGetRepoFlag = false;

    private static RequestQueue mRequestQueue;

    public static Bitmap mAvatarBitMap;

    //TODO: get rid of non constant statics

    @Override
    public void onCreate() {
        super.onCreate();
        mRequestQueue = Volley.newRequestQueue(this.getBaseContext());
        initializeDatabase();
        checkAndLoginSavedUser();
    }

    public static RequestQueue getRequestsQueue(){
        return mRequestQueue;
    }

    public void checkAndLoginSavedUser(){
        SharedPreferences sharedPreferences = getSharedPreferences(KEY_SHARED_PREFERENCE, MODE_PRIVATE);
        String username = sharedPreferences.getString(KEY_USERNAME_SAVED, "");
        if(!TextUtils.isEmpty(username)){
            mCurrentUser = username;
            mCurrentUserPassword = sharedPreferences.getString(KEY_PASSWORD_SAVED, "");
            Log.d("test", "USER: " + mCurrentUser + " has been loaded at start");
            Log.d("test", "PASSWORD: " + mCurrentUserPassword + " has been loaded at start");
        }
    }

    private void initializeDatabase(){
        mDatabase = Room.databaseBuilder(getApplicationContext(),
                GitHubAppDatabase.class, "github_db").build();
    }
}
