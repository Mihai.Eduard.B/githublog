package android.training.githubtrainingapp.utils;

import java.util.Arrays;

public enum DialogSortEnum {CREATED, UPDATED, PUSHED, FULLNAME;

    public static String[] getValuesAsStringArray() {
        String valuesStr = Arrays.toString(DialogSortEnum.values());
        return valuesStr.substring(1, valuesStr.length()-1).replace(" ", "").split(",");
    }
}


