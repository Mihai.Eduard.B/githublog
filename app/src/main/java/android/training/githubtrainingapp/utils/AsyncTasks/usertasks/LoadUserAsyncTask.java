package android.training.githubtrainingapp.utils.AsyncTasks.usertasks;

import android.os.AsyncTask;
import android.training.githubtrainingapp.model.User;
import android.training.githubtrainingapp.utils.GitHubApp;
import android.util.Log;

public class LoadUserAsyncTask extends AsyncTask<String,Void,User> {

    private OnLoadUserListener mOnLoadUserListener;

    public interface OnLoadUserListener{
        public void OnLoadUserFinished(User user);
    }

    public LoadUserAsyncTask(OnLoadUserListener loadUserListener) {
        mOnLoadUserListener = loadUserListener;
    }

    @Override
    protected User doInBackground(String... userNames) {
        User user = GitHubApp.mDatabase.mUserDao().findByUsername(userNames[0]);

        return user;
    }

    @Override
    protected void onPostExecute(User user) {
        super.onPostExecute(user);
        mOnLoadUserListener.OnLoadUserFinished(user);
    }
}
