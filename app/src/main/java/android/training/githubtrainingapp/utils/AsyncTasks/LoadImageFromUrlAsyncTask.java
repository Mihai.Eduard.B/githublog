package android.training.githubtrainingapp.utils.AsyncTasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;

public class LoadImageFromUrlAsyncTask extends AsyncTask<String,Void, Bitmap> {
    private OnImageLoadFromUrlListener mOnImageLoadFromUrlListener;

    public LoadImageFromUrlAsyncTask(OnImageLoadFromUrlListener onImageLoadFromUrlListener) {
        mOnImageLoadFromUrlListener = onImageLoadFromUrlListener;
    }

    public interface OnImageLoadFromUrlListener{
        void onImageLoadFinished(Bitmap bitmap);
    }

    @Override
    protected Bitmap doInBackground(String... strings) {
        String avatarURL = strings[0];
        Bitmap avatar = null;
        try {
            InputStream in = new java.net.URL(avatarURL).openStream();
            avatar = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.d("test", "LOAD IMAGE ASYNC TASK ERROR: " + e.getMessage());
            e.printStackTrace();
        }
        return avatar;
    }

    protected void onPostExecute(Bitmap result) {
        super.onPostExecute(result);
        mOnImageLoadFromUrlListener.onImageLoadFinished(result);
    }
}
