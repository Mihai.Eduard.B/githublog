package android.training.githubtrainingapp.utils.AsyncTasks.usertasks;

import android.os.AsyncTask;
import android.training.githubtrainingapp.model.User;
import android.training.githubtrainingapp.utils.GitHubApp;
import android.util.Log;

public class SaveUserAsyncTask extends AsyncTask<User,Void,Void> {

    private OnSaveUserListener mOnSaveUserListener;

    public SaveUserAsyncTask(OnSaveUserListener onSaveUserListener) {
        mOnSaveUserListener = onSaveUserListener;
    }

    public interface OnSaveUserListener{
        void onSaveUserFinished();
    }


    @Override
    protected Void doInBackground(User... user) {
        if(GitHubApp.mDatabase.mUserDao().findByUsername(user[0].getUsername())==null){
            GitHubApp.mDatabase.mUserDao().insertAll(user);
        }
        //test
        Log.d("test", "Added user: " + GitHubApp.mDatabase.mUserDao().findByUsername(user[0].getUsername()).toString());
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        mOnSaveUserListener.onSaveUserFinished();
    }
}
