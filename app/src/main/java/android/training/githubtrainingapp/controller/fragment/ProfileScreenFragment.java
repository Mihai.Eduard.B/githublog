package android.training.githubtrainingapp.controller.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.training.githubtrainingapp.R;
import android.training.githubtrainingapp.model.EmailModel;
import android.training.githubtrainingapp.model.Repo;
import android.training.githubtrainingapp.model.User;
import android.training.githubtrainingapp.utils.AsyncTasks.LoadImageFromUrlAsyncTask;
import android.training.githubtrainingapp.utils.AsyncTasks.repotasks.SaveReposAsyncTask;
import android.training.githubtrainingapp.utils.GitHubApp;
import android.training.githubtrainingapp.utils.RepoTypeEnum;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class ProfileScreenFragment extends Fragment {

    public static final String KEY_PROFILE_FRAGMENT_USER = "key_profile_fragment_user";
    public static final String TAG_PROFILE_SCREEN_FRAGMENT = "tag_profile_screen_fragment";
    public static final String TAG_GET_REPOS_REQUEST = "tag_get_repos_request";

    private User mCurrentUser;
    private ImageView mProfileAvatar;
    private TextView mProfileBioText;
    private TextView mProfileLocationText;
    private TextView mProfileEmailText;
    private TextView mProfileCreatedText;
    private TextView mProfileUpdatedText;
    private TextView mProfilePublicReposCountText;
    private TextView mProfilePrivateReposCountText;
    private boolean mIsLandscape = false;
    private RepoTypeEnum mRepoTypeEnum = RepoTypeEnum.ALL;  //default

    private Button mViewReposButton;
    private Button mContactByEmailButton;

    LoadImageFromUrlAsyncTask mLoadImageFromUrlAsyncTask = new LoadImageFromUrlAsyncTask(new LoadImageFromUrlAsyncTask.OnImageLoadFromUrlListener() {
        @Override
        public void onImageLoadFinished(Bitmap bitmap) {
            mProfileAvatar.setImageBitmap(bitmap);
            GitHubApp.mAvatarBitMap = bitmap;
        }
    });

    SaveReposAsyncTask saveReposAsyncTask = new SaveReposAsyncTask(() -> {
        startRepoListFragment(mRepoTypeEnum);
        GitHubApp.mGetRepoFlag = false;
    });

    public static ProfileScreenFragment newInstance(User user) {
        Bundle args = new Bundle();

        args.putSerializable(KEY_PROFILE_FRAGMENT_USER, user);
        ProfileScreenFragment fragment = new ProfileScreenFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        setRetainInstance(true);
        mCurrentUser = (User) getArguments().getSerializable(KEY_PROFILE_FRAGMENT_USER);
        if (mCurrentUser != null) {
            Log.d("test", "PROFILE SCREEN FRAGMENT STARTED WITH USER: " + mCurrentUser.toString());
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (Objects.requireNonNull(getActivity()).getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mIsLandscape = true;
        }else{
            mIsLandscape = false;
        }
        View view = inflater.inflate(R.layout.profile_screen_fragment, null);
        view.post(() -> {
            if (mIsLandscape) {
                setBioTextViewSize();
                int profileAvatarPaddingTop = 100;
                mProfileAvatar.setPadding(0, profileAvatarPaddingTop, 0, 0);
            }
        });
        mProfileAvatar = view.findViewById(R.id.profile_avatar_image_view);
        mProfileBioText = view.findViewById(R.id.bio_text_content);
        mProfileLocationText = view.findViewById(R.id.profile_location_text);
        mProfileEmailText = view.findViewById(R.id.profile_email_text);
        mProfileCreatedText = view.findViewById(R.id.profile_created_text);
        mProfileUpdatedText = view.findViewById(R.id.profile_updated_text);
        mProfilePublicReposCountText = view.findViewById(R.id.profile_public_repos_text);
        mProfilePublicReposCountText.setOnClickListener((v) -> {
            getUserRepos(RepoTypeEnum.PUBLIC);
        });

        mProfilePrivateReposCountText = view.findViewById(R.id.private_repos_text);
        mProfilePrivateReposCountText.setOnClickListener(v -> {
            getUserRepos(RepoTypeEnum.PRIVATE);
        });

        mViewReposButton = view.findViewById(R.id.view_public_repos_button);
        mViewReposButton.setOnClickListener((v) -> {
            getUserRepos(RepoTypeEnum.ALL);
        });

        mContactByEmailButton = view.findViewById(R.id.contact_by_email_button);
        mContactByEmailButton.setOnClickListener((v -> {
            EmailModel emailModel = new EmailModel(mCurrentUser.getEmail(), mCurrentUser.getBio(), "I want to test this");
            contactByEmail(emailModel);
        }));

        updateUI();

        return view;
    }

    @Override
    public void onStop () {
        super.onStop();
        GitHubApp.getRequestsQueue().cancelAll(TAG_GET_REPOS_REQUEST);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLoadImageFromUrlAsyncTask.cancel(true);
    }

    private void updateUI() {

        if (mCurrentUser.getBio() == null) {
            mProfileBioText.setText(R.string.bio_placeholder_text);
            mProfileBioText.setTypeface(null, Typeface.ITALIC);
        } else {
            mProfileBioText.setText(mCurrentUser.getBio());
        }
        mProfileLocationText.setText(mCurrentUser.getLocation());
        mProfileEmailText.setText(mCurrentUser.getEmail());
        mProfileCreatedText.setText(mCurrentUser.getCreated_at());
        mProfileUpdatedText.setText(mCurrentUser.getUpdated_at());
        mProfilePublicReposCountText.setText(String.valueOf(mCurrentUser.getPublic_repos()));
        mProfilePrivateReposCountText.setText(String.valueOf(mCurrentUser.getTotal_private_repos()));


        if (GitHubApp.mAvatarBitMap != null) {
            mProfileAvatar.setImageBitmap(GitHubApp.mAvatarBitMap);
        } else {
            if (mCurrentUser.getAvatar_url() != null) {
                mLoadImageFromUrlAsyncTask.execute(mCurrentUser.getAvatar_url());
            }
        }
    }

    private void contactByEmail(EmailModel emailModel) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setDataAndType(Uri.parse("mailto:" + emailModel.getRecipient()), "text/plain");
        Log.d("test", "Parsed uri: " + "mailto:" + emailModel.getRecipient());
        intent.putExtra(Intent.EXTRA_SUBJECT, emailModel.getSubject());
        intent.putExtra(Intent.EXTRA_TEXT, emailModel.getBody());
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            startActivity(Intent.createChooser(intent, "Send Email using:"));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "No email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    private void getUserRepos(RepoTypeEnum repoTypeEnum) {
        if (GitHubApp.mGetRepoFlag) {
            String url = SplashScreenFragment.BASEURL + "/user/repos";
            JsonArrayRequest getCurrentUserReposRequest = new JsonArrayRequest(Request.Method.GET, url, null, response -> {
                Type listType = new TypeToken<List<Repo>>() {
                }.getType();
                Gson gson = new Gson();
                List<Repo> repoList = gson.fromJson(response.toString(), listType);
                for (Repo repo : repoList) {
                    repo.setRepoOwner(GitHubApp.mCurrentUser);
                }
                persistRepoList(repoList, repoTypeEnum);
                Log.d("test", repoList.toString());
            },
                    error -> {
                        error.getCause();
                        if (Objects.requireNonNull(error).networkResponse.statusCode == 401) {
                            //redirect to splash screen
                            Log.d("test", "USERNAME OR PASSWORD IS INVALID");
                        } else {
                            if (Objects.requireNonNull(error).networkResponse.statusCode == 404) {
                                //redirect to splash screen ???
                                Log.d("test", "USER NOT FOUND OR INVALID (GITHUB IS SPECIAL)");
                            }
                        }
                    }) {
                @Override
                public Response<JSONArray> parseNetworkResponse(NetworkResponse response) {
                    if (response.statusCode == 200) {
                        // nothing atm
                    }
                    return super.parseNetworkResponse(response);
                }

                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    String credentials = GitHubApp.mCurrentUser + ":" + GitHubApp.mCurrentUserPassword;
                    Log.d("test", credentials + " for repos");
                    String auth = "Basic "
                            + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                    headers.put("Content-Type", "application/json");
                    Log.d("test", auth);
                    headers.put("Authorization", auth);
                    return headers;
                }
            };
            getCurrentUserReposRequest.setTag(TAG_GET_REPOS_REQUEST);
            GitHubApp.getRequestsQueue().add(getCurrentUserReposRequest);
        } else {
            startRepoListFragment(repoTypeEnum);      //user already has info in db
        }
    }

    private void persistRepoList(List<Repo> repoList, RepoTypeEnum repoTypeEnum) {
        saveReposAsyncTask.execute(repoList.toArray(new Repo[0]));
        mRepoTypeEnum = repoTypeEnum;
    }

    private void startRepoListFragment(RepoTypeEnum repoTypeEnum) {
        FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
        if(fragmentManager.findFragmentByTag(RepoListScreenFragment.TAG_REPO_LIST_SCREEN)==null){
            fragmentManager.beginTransaction()
                    .replace(R.id.profile_fragment_container, RepoListScreenFragment.newInstance(repoTypeEnum), RepoListScreenFragment.TAG_REPO_LIST_SCREEN)
                    .addToBackStack(null)
                    .commit();
        }
    }

    private void setBioTextViewSize() {
        mProfileBioText.setText("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
        int horizontalPadding = 100;
        int bioTextMaxViewWidth = Objects.requireNonNull(getView()).getWidth() - mProfileAvatar.getWidth() - horizontalPadding;
        mProfileBioText.setMaxWidth(bioTextMaxViewWidth);
        Log.d("test", "BIO WIDTH IS: " + bioTextMaxViewWidth);
    }
}
