package android.training.githubtrainingapp.controller.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.training.githubtrainingapp.R;
import android.training.githubtrainingapp.model.Repo;
import android.training.githubtrainingapp.utils.DialogSortEnum;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class RepoSettingsFragment extends Fragment {
    public static final String KEY_SORTING_OPTIONS_ARRAY = "key_sorting_options_array";
    public static final String EXTRA_SORT_TYPE = "extra_sort_type";

    private TextView mSortListTextView;
    private TextView mFilterByAffiliationTextView;

    private CheckBox mCreatedOnCheckBox;
    private CheckBox mUpdatedAtCheckBox;
    private CheckBox mPushedAtCheckBox;
    private CheckBox mFullNameCheckBox;
    private List<CheckBox> mCheckBoxList;
    private DialogSortEnum mDialogSortEnum;
    private String mSortTypeSelected;
    private String[] mSortingOptions;

    private LinearLayout mDialogSortView;

    public static RepoSettingsFragment newInstance(String[] sortingOptions) {

        Bundle args = new Bundle();

        args.putStringArray(KEY_SORTING_OPTIONS_ARRAY, sortingOptions);

        RepoSettingsFragment fragment = new RepoSettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mSortingOptions = getArguments().getStringArray(KEY_SORTING_OPTIONS_ARRAY);
        }

        assert mSortingOptions != null;
        Log.d("test", "SORTING OPTIONS ARE: " + Arrays.toString(mSortingOptions));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.repo_setting_fragment, null);

        mSortListTextView = view.findViewById(R.id.sort_list_text_view);
        mSortListTextView.setOnClickListener((v -> {
            setupOnCreateDialogView();
            showSortCriteriaSelectionDialog();
        }));

        mFilterByAffiliationTextView = view.findViewById(R.id.filter_by_affiliation_list_text_view);
        mFilterByAffiliationTextView.setOnClickListener((v -> {
            //
        }));

        return view;

    }

    private void showSortCriteriaSelectionDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("");
        builder.setMessage(" Sort By ")
                .setView(mDialogSortView)
                .setCancelable(false)
                .setPositiveButton("Yes", (dialog, id) -> {
                    Toast.makeText(getActivity(), "WOLOLO", Toast.LENGTH_SHORT).show();
                    sendRepoSortTypeString(Activity.RESULT_OK, mSortTypeSelected);
                })
                .setNegativeButton("No", (dialog, id) -> dialog.cancel())
                .show();
    }

    private void setupOnCreateDialogView() {
        mDialogSortView = (LinearLayout) View.inflate(getActivity(), R.layout.sort_repo_list_dialog, null);
        mCheckBoxList = new ArrayList<>();

        for (String sortOption : mSortingOptions) {
            CheckBox checkBox = new CheckBox(getActivity());
            checkBox.setText(sortOption);
            checkBox.setOnCheckedChangeListener(((buttonView, isChecked) -> {
                if (isChecked) {
                    mSortTypeSelected = sortOption;
                    uncheckExtraBoxes(checkBox);
                    Log.d("test", "CLICKED ON: " + mSortTypeSelected);
                }
            }));
            mCheckBoxList.add(checkBox);
            mDialogSortView.addView(checkBox);
        }
    }

    private void uncheckExtraBoxes(CheckBox checkBoxLastClickedOn) {
        for (CheckBox checkbox : mCheckBoxList) {
            if (!checkbox.equals(checkBoxLastClickedOn)) {
                checkbox.setChecked(false);
            }
        }
    }

    private void sendRepoSortTypeString(int resultCode, String sortTypeSelected) {
        if (getTargetFragment() == null) {
            Log.d("test", "NO TARGET FRAGMENT FOUND FOR REPO SETTINGS FRAGMENT");
        } else {
            Intent intent = new Intent();
            intent.putExtra(EXTRA_SORT_TYPE, sortTypeSelected);

            getTargetFragment().onActivityResult(getTargetRequestCode(), resultCode, intent);
        }


    }

}
