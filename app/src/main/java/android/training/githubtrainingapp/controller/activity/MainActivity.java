package android.training.githubtrainingapp.controller.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.training.githubtrainingapp.R;
import android.training.githubtrainingapp.controller.fragment.SplashScreenFragment;
import android.training.githubtrainingapp.utils.GitHubApp;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!GitHubApp.mCurrentUser.equals("")){
            startProfileScreenActivity();
        }else {
            startFragment(SplashScreenFragment.newInstance());
            GitHubApp.mGetRepoFlag = true;
        }

    }

    private void startFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .add(R.id.main_fragment_container, fragment)
                .commit();
    }

    private void startProfileScreenActivity(){
        Intent intent = new Intent(this, ProfileScreenActivity.class);
        startActivity(intent);

    }
}
