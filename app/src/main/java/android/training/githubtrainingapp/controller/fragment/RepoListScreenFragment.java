package android.training.githubtrainingapp.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.training.githubtrainingapp.R;
import android.training.githubtrainingapp.model.Repo;
import android.training.githubtrainingapp.utils.AsyncTasks.repotasks.LoadReposAsyncTask;
import android.training.githubtrainingapp.utils.DialogSortEnum;
import android.training.githubtrainingapp.utils.GitHubApp;
import android.training.githubtrainingapp.utils.RepoListRecycleViewAdapter;
import android.training.githubtrainingapp.utils.RepoTypeEnum;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class RepoListScreenFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RepoTypeEnum mRepoTypeEnum;
    private SearchView mSearchView;
    private boolean hasBeenDetached = false;

    private List<Repo> mRepoList;

    public static final String KEY_REPO_ENUM_TYPE = "key_repo_enum_type";
    public static final int REQUEST_SORT_TYPE = 99;
    public static final String TAG_REPO_LIST_SCREEN = "tag_repo_enum_type";


    LoadReposAsyncTask mLoadReposAsyncTask = new LoadReposAsyncTask(repos -> {
        mRepoList = repos;
        Log.d("test", "REPOS FRM DB: " + mRepoList.toString());
        setRecycleViewAdapter();
    });


    public static RepoListScreenFragment newInstance(RepoTypeEnum repoType) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_REPO_ENUM_TYPE, repoType);
        RepoListScreenFragment fragment = new RepoListScreenFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(hasBeenDetached){
            resetList();
        }
        Log.d("test", "ON ATTACH REPO LIST CALLED");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        assert getArguments() != null;
        mRepoTypeEnum = (RepoTypeEnum) getArguments().getSerializable(KEY_REPO_ENUM_TYPE);
        Log.d("test", "ENUM TYPE IS: " + mRepoTypeEnum);
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.repo_list_screen_fragment, null);

        if (Objects.requireNonNull(getActivity()).getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setupRepoListRecycleView(view);  //land_repo_detail_fragment_container


            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.land_repo_detail_fragment_container, RepoDetailFragment.getInstance("Click on a repository from the left to view it's details"))
                    .commit();

        } else { //portrait
            setupRepoListRecycleView(view);
        }

        if (mRepoList == null) {
            mLoadReposAsyncTask.execute(GitHubApp.mCurrentUser, mRepoTypeEnum.toString());
        } else {
            setRecycleViewAdapter();
        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.repo_list_settings_menu, menu);


        MenuItem menuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) menuItem.getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String searchViewText) {
                RepoListRecycleViewAdapter adapter = (RepoListRecycleViewAdapter) mRecyclerView.getAdapter();
                assert adapter != null;
                adapter.getFilter().filter(searchViewText);
                return false;
            }
        });
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_SORT_TYPE) {
            String sortType = data.getStringExtra(RepoSettingsFragment.EXTRA_SORT_TYPE);
            Log.d("test", "REPO LIST FRAGMENT GOT BACK ON ACTIVITY RESULT SORT TYPE: " + sortType);
            sortRepoList(sortType);
        }
    }

    private void setRecycleViewAdapter() {
        mRecyclerView.setAdapter(new RepoListRecycleViewAdapter(mRepoList));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLoadReposAsyncTask.cancel(true);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        hasBeenDetached = true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Fragment settingsFragment = RepoSettingsFragment.newInstance(DialogSortEnum.getValuesAsStringArray());
                settingsFragment.setTargetFragment(RepoListScreenFragment.this, REQUEST_SORT_TYPE);

                FragmentManager fragmentManager = Objects.requireNonNull(getActivity()).getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.profile_fragment_container, settingsFragment)
                        .addToBackStack(null)
                        .commit();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupRepoListRecycleView(View view) {
        mRecyclerView = view.findViewById(R.id.repo_list_recycle_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public List<Repo> getRepoList() {
        return mRepoList;
    }

    public void setRepoList(List<Repo> repoList) {
        mRepoList = repoList;
    }

    private void sortRepoList(String sortType) {
        if (sortType != null) {
            DialogSortEnum sortEnum = DialogSortEnum.valueOf(sortType);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                switch (sortEnum) {
                    case CREATED:
                        mRepoList.sort(Comparator.comparing(Repo::getCreated_at));
                        break;
                    case UPDATED:
                        mRepoList.sort(Comparator.comparing(Repo::getUpdated_at));
                        break;
                    case PUSHED:
                        mRepoList.sort(Comparator.comparing(Repo::getPushed_at));
                        break;
                    case FULLNAME:
                        mRepoList.sort(Comparator.comparing(Repo::getFull_name));
                        break;
                    default:
                        Log.d("test", "NO CHECKBOX WAS CHECKED");
                }
            }
        }
    }
    private void resetList() {
        RepoListRecycleViewAdapter adapter = (RepoListRecycleViewAdapter) mRecyclerView.getAdapter();
        assert adapter != null;
        adapter.getFilter().filter("");
    }
}
