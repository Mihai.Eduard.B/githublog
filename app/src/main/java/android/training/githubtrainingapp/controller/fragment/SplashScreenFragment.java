package android.training.githubtrainingapp.controller.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.training.githubtrainingapp.R;
import android.training.githubtrainingapp.controller.activity.ProfileScreenActivity;
import android.training.githubtrainingapp.model.User;
import android.training.githubtrainingapp.utils.GitHubApp;
import android.training.githubtrainingapp.utils.AsyncTasks.usertasks.SaveUserAsyncTask;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SplashScreenFragment extends Fragment {   //user AndrTest

    public static final String BASEURL = "https://api.github.com";
    public static final String TAG_GET_USER_REQUEST = "tag_get_user_request";

    private EditText mUserEditText;
    private EditText mPasswordEditText;
    private Button mButton;
    private String mUsername = "";
    private String mPassword = "";

    public static SplashScreenFragment newInstance() {
        Bundle args = new Bundle();

        SplashScreenFragment fragment = new SplashScreenFragment();
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.splash_screen_fragment, null);


        mButton = view.findViewById(R.id.login_button);
        mButton.setOnClickListener((v) -> {
            sendLoginRequest();
        });
        mUserEditText = view.findViewById(R.id.user_name_edit);
        mUserEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mPasswordEditText = view.findViewById(R.id.user_password_edit);
        mPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("test", mPasswordEditText.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        return view;
    }

    @Override
    public void onStop () {
        super.onStop();
        GitHubApp.getRequestsQueue().cancelAll(TAG_GET_USER_REQUEST);
    }

    private void sendLoginRequest() {
        mUsername = mUserEditText.getText().toString();
        mPassword = mPasswordEditText.getText().toString();

        Log.d("test", mUsername + " " + mPassword);

        if (validCredentials(mUsername, mPassword)) {

            if (checkForInternetConnectivity()) {

                executeLoginBasicAuthRequest();
            } else {
                Toast.makeText(getActivity(), "No internet Connectivity", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void executeLoginBasicAuthRequest() {
        String url = BASEURL + "/user";
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.GET, url, null, response -> {
            try {
                User loggedInUser = mapJsonToUser(response);
                GitHubApp.mCurrentUser = loggedInUser.getUsername();
                saveValidUserToSharedPrefs(GitHubApp.mCurrentUser);

                SaveUserAsyncTask saveUserAsyncTask = new SaveUserAsyncTask(()->{
                    Intent intent = new Intent(getActivity(), ProfileScreenActivity.class);
                    intent.putExtra(GitHubApp.KEY_USERNAME_TO_PROFILE_SCREEN, loggedInUser.getUsername());

                    startActivity(intent);
                });
                saveUserAsyncTask.execute(loggedInUser);

            } catch (Exception e) {
                e.printStackTrace();
            }
        },
                error -> {
                    Log.d("test", "ERROR CODE IS: " + error.networkResponse.statusCode);
                    if (error.networkResponse.statusCode == 401) {
                        Log.d("test", "USERNAME OR PASSWORD IS INVALID");
                    } else {
                        if (error.networkResponse.statusCode == 404) {
                            Log.d("test", "USER NOT FOUND OR INVALID (GITHUB IS SPECIAL)");
                        }
                    }
                }) {
            @Override
            public Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                if (response.statusCode == 200) {
                    /// nothing atm
                }
                return super.parseNetworkResponse(response);
            }

            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String credentials = mUsername + ":" + mPassword;
                Log.d("test", "credentials: " + credentials);
                String auth = "Basic "
                        + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
                headers.put("Content-Type", "application/json");
                Log.d("test", auth);
                headers.put("Authorization", auth);
                return headers;
            }
        };
        jsonRequest.setTag(TAG_GET_USER_REQUEST);
        GitHubApp.getRequestsQueue().add(jsonRequest);
    }


    private User mapJsonToUser(JSONObject jsonObject) {
        User user = new Gson().fromJson(jsonObject.toString(), User.class);
        user.setUsername(mUsername);

        return user;
    }

    private boolean validCredentials(String username, String password) {

        if (!username.matches("^[a-zA-Z0-9]+([a-zA-Z0-9]([_\\- ])[a-zA-Z0-9])*[a-zA-Z0-9]{3,15}")) {
            invalidUsernameResponse();
            return false;
        } else if (!password.matches("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$")) {
            invalidPasswordResponse();
            return false;
        } else {
            return true;
        }
    }

    private void invalidUsernameResponse() {
        Toast.makeText(getActivity(), getString(R.string.invalid_username_message), Toast.LENGTH_LONG).show();
        mUserEditText.setText("");
        mPasswordEditText.setText("");
    }

    private void invalidPasswordResponse() {
        Toast.makeText(getActivity(), getString(R.string.invalid_password), Toast.LENGTH_LONG).show();
        mPasswordEditText.setText("");
    }

    private boolean checkForInternetConnectivity() {
        ConnectivityManager cm = (ConnectivityManager) Objects.requireNonNull(getActivity()).getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null &&
                activeNetwork.isConnected();
    }

    private void saveValidUserToSharedPrefs(String currentUser) {
        SharedPreferences sharedPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(GitHubApp.KEY_SHARED_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(GitHubApp.KEY_USERNAME_SAVED, currentUser);
        editor.putString(GitHubApp.KEY_PASSWORD_SAVED, mPassword);
        editor.apply();
        GitHubApp.mCurrentUserPassword = mPassword;
        Log.d("test", "Username saved is: " + currentUser);
    }

}



