package android.training.githubtrainingapp.controller.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.training.githubtrainingapp.R;
import android.training.githubtrainingapp.controller.fragment.ProfileScreenFragment;
import android.training.githubtrainingapp.controller.fragment.RepoDetailFragment;
import android.training.githubtrainingapp.model.User;
import android.training.githubtrainingapp.utils.AsyncTasks.usertasks.DeleteUserAndReposAsyncTask;
import android.training.githubtrainingapp.utils.GitHubApp;
import android.training.githubtrainingapp.utils.AsyncTasks.usertasks.LoadUserAsyncTask;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class ProfileScreenActivity extends AppCompatActivity {

    private String mCurrentUsername;

    private User mCurrentUser;

    DeleteUserAndReposAsyncTask deleteUserAsyncTask = new DeleteUserAndReposAsyncTask(() -> {
        GitHubApp.mCurrentUser = "";
        GitHubApp.mCurrentUserPassword = "";
        startActivity(new Intent(this, MainActivity.class));
        this.finish();
    });

    LoadUserAsyncTask loadUserAsyncTask = new LoadUserAsyncTask(new LoadUserAsyncTask.OnLoadUserListener() {
        @Override
        public void OnLoadUserFinished(User user) {
            mCurrentUser = user;
            startProfileScreenFragment();
            if (mCurrentUser != null) {
                Log.d("test", "CURRENT USER IN PROFILE ACTIVITY IS: " + mCurrentUser.toString());
            }
        }
    });

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_screen_activity);
        mCurrentUsername = getIntent().getStringExtra(GitHubApp.KEY_USERNAME_TO_PROFILE_SCREEN);
        loadUserAsyncTask.execute(GitHubApp.mCurrentUser);
        Log.d("Test", "USER IN PROFILE SCREEN IN PORTRAIT= " + GitHubApp.mCurrentUser);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            Fragment profileScreenFragment = fragmentManager.findFragmentByTag(ProfileScreenFragment.TAG_PROFILE_SCREEN_FRAGMENT);
           if(profileScreenFragment!=null && profileScreenFragment.isVisible()){

            Log.d("test", "INVISIBLE SET TO DETAIL FRAGMENT CONTAINER");
           }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.profile_screen_menu, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        deleteUserAsyncTask.cancel(true);
        loadUserAsyncTask.cancel(true);
        super.onDestroy();
    }

    private void logOutCurrentUser() {
        SharedPreferences sharedPreferences = getSharedPreferences(GitHubApp.KEY_SHARED_PREFERENCE, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().apply();
        deleteUserAsyncTask.execute(mCurrentUser);
        GitHubApp.mAvatarBitMap = null;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout_action_menu_item:
                logOutCurrentUser();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    private void startProfileScreenFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ProfileScreenFragment fragment =(ProfileScreenFragment) fragmentManager.findFragmentByTag(ProfileScreenFragment.TAG_PROFILE_SCREEN_FRAGMENT);
        if(fragment==null){
            fragmentManager.beginTransaction()
                    .replace(R.id.profile_fragment_container, ProfileScreenFragment.newInstance(mCurrentUser), ProfileScreenFragment.TAG_PROFILE_SCREEN_FRAGMENT)
                    .addToBackStack(null)
                    .commit();
        }else{
            // probably nothing
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment repoDetailFragment = fragmentManager.findFragmentByTag(RepoDetailFragment.TAG_REPO_DETAIL_FRAGMENT);
        if (repoDetailFragment != null) {
            fragmentManager.beginTransaction()
                    .remove(repoDetailFragment)
                    .commit();
        }
    }
}
