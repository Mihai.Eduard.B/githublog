package android.training.githubtrainingapp.controller.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.training.githubtrainingapp.R;
import android.training.githubtrainingapp.model.Repo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Objects;

public class RepoDetailFragment extends Fragment {
    public static final String KEY_REPO_DETAIL_INSTANCE = "key_repo_detail_instance";
    public static final String KEY_DEFAULT_MESSAGE_STRING = "key_default_message_string";
    public static final String TAG_REPO_DETAIL_FRAGMENT = "tag_repo_detail_fragment";

    private Repo mRepo;
    private String mDefaultMessage;
    private TextView mFragmentDetailsTextView;


    public static RepoDetailFragment getInstance(Repo repo) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_REPO_DETAIL_INSTANCE, repo);


        RepoDetailFragment repoDetailFragment = new RepoDetailFragment();
        repoDetailFragment.setArguments(args);
        return repoDetailFragment;
    }

    public static RepoDetailFragment getInstance(String repoString) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_DEFAULT_MESSAGE_STRING, repoString);

        RepoDetailFragment repoDetailFragment = new RepoDetailFragment();
        repoDetailFragment.setArguments(args);
        return repoDetailFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        assert getArguments() != null;
        mRepo = (Repo) getArguments().getSerializable(KEY_REPO_DETAIL_INSTANCE);
        mDefaultMessage = getArguments().getString(KEY_DEFAULT_MESSAGE_STRING);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.repo_detail_fragment, null);
        mFragmentDetailsTextView = view.findViewById(R.id.repo_details_text);
        if(mDefaultMessage!=null){
            mFragmentDetailsTextView.setText(mDefaultMessage);
        }else{
            mFragmentDetailsTextView.setText(mRepo.getName() + " " + mRepo.getRepoOwner() + " Private Repo: " + mRepo.isPrivate());
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // do something here
    }
}
