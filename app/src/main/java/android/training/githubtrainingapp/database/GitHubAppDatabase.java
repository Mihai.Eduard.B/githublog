package android.training.githubtrainingapp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.training.githubtrainingapp.model.Repo;
import android.training.githubtrainingapp.model.User;

@Database(entities ={User.class, Repo.class}, version = 1, exportSchema = false)
public abstract class GitHubAppDatabase extends RoomDatabase {
    public abstract UserDao mUserDao();
    public abstract RepoDao mRepoDao();
}
