package android.training.githubtrainingapp.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.training.githubtrainingapp.model.User;

import java.util.List;

@Dao
public interface UserDao {

    @Query("Select * FROM user")
    List<User> getAllUsers();

    @Query("SELECT * FROM user WHERE mID IN (:userIds)")
    List<User> loadAllByIds(int[] userIds);

    @Query("SELECT * FROM user WHERE username LIKE :username LIMIT 1")
    User findByUsername(String username);

    @Insert
    void insertAll(User... users);

    @Delete
    void delete(User user);
}
