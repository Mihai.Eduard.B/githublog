package android.training.githubtrainingapp.database;

import android.arch.paging.DataSource;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.training.githubtrainingapp.model.Repo;

import java.util.List;

@Dao
public interface RepoDao {

    @Query("Select * FROM repo")
    List<Repo> getAllRepos();

    @Query("SELECT * FROM repo WHERE mID IN (:repoIds)")
    List<Repo> loadAllByIds(int[] repoIds);

    @Query("SELECT * FROM repo WHERE name LIKE :name LIMIT 1")
    Repo findByname(String name);

    @Query("SELECT * FROM repo WHERE repoOwner IN (:repoOwner)")
    List<Repo> findAllByOwner(String repoOwner);

    @Query("SELECT * FROM repo WHERE repoOwner IN (:repoOwner) AND isPrivate IN (:isPrivate)")
    List<Repo> findAllByNameAndPrivacy(String repoOwner, boolean isPrivate);

    @Query("Select * FROM repo WHERE name LIKE '%' || :searchString || '%'")
    List<Repo> findAllReposByWildcardName(String searchString);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Repo... repos);

    @Delete
    void delete(Repo user);

    @Query("DELETE FROM repo")
    void deleteAll();
}
